#!/bin/sh

./mpls_tun -m l3vpn -d br0 -D br0 -i 17 -o 16 -I 00:1e:f7:9e:40:71 -O 00:1f:9d:44:8c:c0 -v &
sleep 2
ifconfig tun0 172.31.2.11/24
ifconfig tun0 mtu 1300
route add -net 172.31.1.0/24 dev tun0
ping 172.31.1.1
